FROM debian:stretch

# A bit from https://www.ekito.fr/people/run-a-cron-job-with-docker/
# and https://stackoverflow.com/questions/37458287/how-to-run-a-cron-job-inside-a-docker-container
# THE ONLY ONE: https://crontab.guru

RUN apt update && \
    apt install -y transmission-cli && \
    apt install -y cron && \
    apt clean && \
    rm -r /var/lib/apt && \
    rm -r /var/cache/apt

COPY gc.sh /root/gc.sh

RUN chmod 0744 /root/gc.sh && \
    echo "0 * * * * bash -c '. /root/env; /root/gc.sh > /proc/1/fd/1 2> /proc/1/fd/2'" > /etc/cron.d/transmission-gc && \
    chmod 0644 /etc/cron.d/* && \
    crontab /etc/cron.d/transmission-gc

VOLUME /downloading /downloaded

#Preserves the environment variables for the cron
CMD \
    echo export TRANS_HOST=$TRANS_HOST >> /root/env && \
    echo export TRANS_AUTH=$TRANS_AUTH >> /root/env && \
    echo export TRANS_FINAL_PATH=$TRANS_FINAL_PATH >> /root/env && \
    echo export FILES_USER=$FILES_USER >> /root/env && \
    echo export FILES_GROUP=$FILES_GROUP >> /root/env && \
    echo export RATIO=$RATIO >> /root/env && \
    chmod +x /root/env && \
    cron -f
