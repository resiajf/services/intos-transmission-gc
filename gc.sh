#!/bin/bash

# Based from https://gist.github.com/onedr0p/8fd8455f08f4781cad9e01a1d65bc34f
# at 18/07/2018

# Set =~ to be insensitive
shopt -s nocasematch

TRANS_REMOTE_BIN=/usr/bin/transmission-remote
[[ -z "${TRANS_HOST}" ]] && TRANS_HOST="127.0.0.1:9091"
[[ -z "${TRANS_AUTH}" ]] && TRANS_AUTH="admin:admin"

# Final directory
[[ -z "${TRANS_FINAL_PATH}" ]] && TRANS_FINAL_PATH="/downloaded"

# User and group for the files
[[ -z "${FILES_USER}" ]] && FILES_USER=1000
[[ -z "${FILES_GROUP}" ]] && FILES_GROUP=1000

# Delete torrents only when ratio is above
[[ -z "${RATIO}" ]] && RATIO="8"

# Executes transmission-remote correctly
function tcli() {
  "${TRANS_REMOTE_BIN}" "${TRANS_HOST}" -n "${TRANS_AUTH}" $@
}

printf "==============================================================================\n  "
date -Iseconds
printf "==============================================================================\n"

# Clean up torrents where trackers have torrent not registered
# filter list by * (which signifies a tracker error)
TORRENT_DEAD_LIST=($(tcli -l | sed -e '1d;$d;s/^ *//' | cut --only-delimited --delimiter=' ' --fields=1 | egrep '[0-9]+\*' | sed 's/\*$//'))
for torrent_id in "${TORRENT_DEAD_LIST[@]}"
do
  # Get the torrents metadata
  torrent_info=$(tcli --torrent "${torrent_id}" -i -it)
  torrent_name=$(echo "${torrent_info}" | grep "Name: *" | sed 's/Name\:\s//i' | awk '{$1=$1};1')
  torrent_path=$(echo "${torrent_info}" | grep "Location: *" | sed 's/Location\:\s//i' | awk '{$1=$1};1')
  torrent_size=$(echo "${torrent_info}" | grep "Downloaded: *" | sed 's/Downloaded\:\s//i' | awk '{$1=$1};1')

  torrent_error=$(echo "${torrent_info}" | grep "Got an error" | cut -d \" -f2)
  if [[ "${torrent_error}" =~ "unregistered" ]] || [[ "${torrent_error}" =~ "not registered" ]]; then
    # Delete torrent
    tcli --torrent "${torrent_id}" --remove-and-delete > /dev/null
  fi
done

# Clean up torrent where ratio is > ${RATIO} or seeding time > ${RETENTION} seconds
# do not filter the list, get all the torrents
TORRENT_ALL_LIST=($(tcli -l | sed -e '1d;$d;s/^ *//' | cut --only-delimited --delimiter=' ' --fields=1))
for torrent_id in "${TORRENT_ALL_LIST[@]}"
do
  # Get the torrents metadata
  torrent_info=$(tcli --torrent "${torrent_id}" -i -it)
  torrent_name=$(echo "${torrent_info}" | grep "Name: *" | sed 's/Name\:\s//i' | awk '{$1=$1};1')
  torrent_path=$(echo "${torrent_info}" | grep "Location: *" | sed 's/Location\:\s//i' | awk '{$1=$1};1')
  torrent_size=$(echo "${torrent_info}" | grep "Downloaded: *" | sed 's/Downloaded\:\s//i' | awk '{$1=$1};1')
  #torrent_seeding_seconds=$(echo "${torrent_info}" | grep "Seeding Time: *" | awk -F"[()]" '{print $2}' | sed 's/\sseconds//i')
  torrent_ratio=$(echo "${torrent_info}" | grep "Ratio: *" | sed 's/Ratio\:\s//i' | awk '{$1=$1};1')
  
  # Debug
  echo "${torrent_id} - ${torrent_ratio} - ${torrent_name}"

  # Torrents without a ratio have "None" instead of "0.0" let's fix that
  if [[ "${torrent_ratio}" =~ "None" ]]; then
    torrent_ratio="0.0"
  fi

  # delete torrents greater than ${RETENTION}
  #if [[ "${torrent_seeding_seconds}" -gt "${RETENTION}" ]]; then
  #  "${TRANS_REMOTE_BIN}" ${TRANS_OPT} --torrent "${torrent_id}" --remove-and-delete > /dev/null
  #fi

  # delete torrents greater than ${RATIO}
  if (( $(echo "${torrent_ratio} ${RATIO}" | awk '{print ($1 >= $2)}') )); then
    echo "  Torrent has reached the ratio"
    echo "    1. Copying from '${torent_path}/${torrent_name}' to '${TRANS_FINAL_PATH}/${torrent_name}'"
    cp -R "${torent_path}/${torrent_name}" "${TRANS_FINAL_PATH}"
    echo "    2. Changing permissions to $FILES_USER:$FILES_GROUP"
    chown -R $FILES_USER:$FILES_GROUP "${TRANS_FINAL_PATH}/${torrent_name}"
    chmod -R 444 "${TRANS_FINAL_PATH}/${torrent_name}"
    echo "    3. Deleting torrent and old file"
    tcli --torrent "${torrent_id}" --remove-and-delete > /dev/null
    echo -e "    4. Done\n"
  else
    echo "  Torrent under ratio limit, nothing to do"
  fi
done